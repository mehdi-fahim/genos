<?php
/**
 * User: mehdifahim
 * Date: 05/10/2018
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ProjectController extends Controller
{
    /**
     * @Route("/project", name="project")
     */
    public function projectAction(Security $security)
    {
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $user = $security->getUser();
            $prenomUser = $user->getPrenom();
            $emailUser = $user->getEmail();
            return $this->render('pages/project.html.twig', array(
                'prenom' => $prenomUser,
                'email' => $emailUser,
            ));

        } else {
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/403", name="error")
     */
    public function errorAction()
    {
        return $this->render('error/403.html.twig');
    }

}
