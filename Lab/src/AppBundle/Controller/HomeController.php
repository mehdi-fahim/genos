<?php
/**
 * User: mehdifahim
 * Date: 03/10/2018
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HomeController extends Controller
{
    /**
     * @Route("/home", name="home")
     */
    public function homeAction(Security $security)
    {
        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {

            $user = $security->getUser();
            $prenomUser = $user->getPrenom();
            $emailUser = $user->getEmail();
            return $this->render('home/home.html.twig', array(
                'prenom' => $prenomUser,
                'email' => $emailUser,
            ));

        } else {

          return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/forgot-pass", name="forgot-pass")
     */
    public function forgotPassAction()
    {
      return $this->render('home/forgot-pass.html.twig');
    }

}
