-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  mer. 27 juin 2018 à 08:45
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `symfony-project`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nb_toilette` int(11) NOT NULL,
  `nb_salle_de_bain` int(11) NOT NULL,
  `nb_chambre` int(11) NOT NULL,
  `nb_personne` int(11) NOT NULL,
  `surface` int(11) NOT NULL,
  `date_creation` datetime NOT NULL,
  `date_ouverture` datetime NOT NULL,
  `date_fermeture` datetime NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id`, `titre`, `description`, `prix`, `adresse`, `ville`, `code_postal`, `pays`, `nb_toilette`, `nb_salle_de_bain`, `nb_chambre`, `nb_personne`, `surface`, `date_creation`, `date_ouverture`, `date_fermeture`, `state`, `image`) VALUES
(1, 'Annonce First', 'Cest la première annonce du site que je viens de mettre à jour', 20, '56 avenue des fraises', 'Paris', '75012', 'France', 3, 2, 2, 4, 50, '2018-06-26 11:38:48', '2018-09-17 00:00:00', '2019-05-13 00:00:00', 'review', ''),
(2, 'Super Annonce', 'Une belle annonce avec une image superbe', 40, '899 rue des bonbon', 'Marseille', '13010', 'France', 1, 1, 2, 4, 50, '2018-06-27 10:06:06', '2019-05-18 00:00:00', '2020-04-01 00:00:00', 'review', 'C:\\Users\\MFAHIM\\AppData\\Local\\Temp\\phpD6CF.tmp'),
(3, 'Super Montagne', 'Je loue une super montagne... enfin une mais dans près d\'une superbe montagne', 65, '14 boulevard des clowns', 'Tremblay', '93290', 'France', 2, 2, 2, 4, 60, '2018-06-27 10:13:02', '2018-01-01 00:00:00', '2019-01-01 00:00:00', 'review', 'C:\\Users\\MFAHIM\\AppData\\Local\\Temp\\php2DED.tmp'),
(4, 'Annonce', 'C\'est encore une annonce test', 42, '156 avenue des fraises', 'Paris', '75013', 'France', 2, 2, 2, 2, 30, '2018-06-27 10:16:46', '2018-02-10 00:00:00', '2018-11-01 00:00:00', 'review', '09f19d6152970066166287200d1b40af.jpeg'),
(5, 'Cabane', 'Belle Cabane dans les arbre', 40, 'Rue des buissons', 'Marseille', '13003', 'France', 1, 1, 1, 2, 30, '2018-06-27 10:23:48', '2018-04-05 00:00:00', '2019-02-05 00:00:00', 'review', '9a5e0a168e1d8fcc315ba55df4784994.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id`, `titre`, `description`, `date_create`, `state`, `image`) VALUES
(1, 'Changement de l\'article1', 'Alors, voici une super description parce-que la précédente ne voulait rien dire.', '2018-06-25 16:08:14', 'review', ''),
(2, 'Voila le 2eme Article', 'Ceci est le 2 eme article moin interéssant mais tout aussi complet.', '2018-06-26 09:52:29', 'review', ''),
(3, 'Article avec une Image', 'On va tester avec une image', '2018-06-26 15:21:09', 'review', '4e0335db6bc8e9a55ce2ccde4f242f1a.jpeg'),
(4, 'Super Article 2018', 'C\'est un bonne article fait en 2018', '2018-06-27 10:03:14', 'review', '15d3f6d73a0d74d3a90034304e7bc47f.jpeg'),
(5, 'AAAAAARTIICCLLLE', 'Une article qui fais du bruit', '2018-06-27 10:21:47', 'review', 'ca7d7e60ac4f5152de0f003aa572a9ce.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6DC044C557698A6A` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `group`
--

INSERT INTO `group` (`id`, `name`, `role`) VALUES
(1, 'Utilisateur', 'ROLE_USER'),
(2, 'Modérateur', 'ROLE_MODO'),
(3, 'Administrateur', 'ROLE_ADMIN');

-- --------------------------------------------------------

--
-- Structure de la table `topic`
--

DROP TABLE IF EXISTS `topic`;
CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `topic`
--

INSERT INTO `topic` (`id`, `titre`, `description`, `theme`, `categorie`, `date_create`, `state`, `image`) VALUES
(1, '15% de son cerveau ?', 'Utilise t-on la totalité de notre cerveau ou alors seulement une partie', 'Question', NULL, '2018-06-26 11:00:04', 'review', NULL),
(2, 'Le lait avant ou après', 'Vous mettez le lait avant ou après les céréales (perso je met après).', 'Débat', NULL, '2018-06-26 11:01:36', 'review', NULL),
(3, 'Rouge ou Bleu', 'Vous preferez le rouge ou le bleu ?', 'Débat', NULL, '2018-06-27 10:36:43', 'review', 'f37897ab11b513c183b994cec1f43a24.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `tutoriel`
--

DROP TABLE IF EXISTS `tutoriel`;
CREATE TABLE IF NOT EXISTS `tutoriel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `pre_requis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `tutoriel`
--

INSERT INTO `tutoriel` (`id`, `titre`, `description`, `pre_requis`, `categorie`, `date_create`, `state`, `image`) VALUES
(1, 'C\'est mon premier tuto', 'Etape 1 : Avoir une bonne idée\r\nEtape 2 : La réaliser', 'Réfléchir un peu ...', NULL, '2018-06-26 10:36:55', 'review', NULL),
(2, 'Tuto très intéressant', 'Ce tuto est très intéressant, suivez le à la lettre', 'Bon en tuto', NULL, '2018-06-26 10:40:06', 'review', NULL),
(3, 'C\'est un tuto pour internet', 'D\'abord téléchanger le logiciel et ensuite l\'installer', 'Savoir installer un logiciel', NULL, '2018-06-27 10:40:32', 'review', '2e903620ab15961def1e0b80c0b9f6b8.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64986CC499D` (`pseudo`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `civilite`, `pseudo`, `nom`, `prenom`, `email`, `password`, `telephone`, `date_create`, `is_active`, `image`, `adresse`, `ville`, `code_postal`, `pays`) VALUES
(2, 'Monsieur', 'SecUser', 'SecondUser', 'UserSecond', 'modo@gmail.com', '$2y$13$B.EduShWSRJ52u9ZMqWL9OOhrWVNx9ITaIAFXD3MvlKQEK8DIpz.m', '0201030605', '2018-06-19 11:56:37', 1, 'www.image-profil.fr', '14 boulevard des clowns', 'Tremblay', '93290', 'France'),
(3, 'Monsieur', 'admin', 'admin', 'admin', 'admin@gmail.com', '$2y$13$QNmeYQ6MILqlxEGW7Ma22epCdCQXnmeXaVPsfmOJh7pJLufMFouru', '0405060208', '2018-06-19 12:40:32', 1, 'www.image-profil.fr', '56 avenue des fraises', 'Paris', '75012', 'France'),
(4, 'Monsieur', 'user', 'User', 'User', 'user@gmail.com', '$2y$13$rYdjoZAn0RlsMWoibr7.F.ygn9BIhakEe3.ruuiYNSFTACSdrWy3m', '0124563693', '2018-06-19 17:28:26', 1, 'www.image-profil.fr', '899 rue des bonbon', 'Marseille', '13010', 'France'),
(5, 'Monsieur', 'First', 'first', 'first', 'first@gmail.com', '$2y$13$MvlSDt9y3HFVik3KJ3pDeu3NSnmp/Vg9K7e6CJ0cSRj5KNVUGZ0FS', '0502060301', '2018-06-22 15:47:11', 1, 'www.image-first.fr', '899 rue des bonbon', 'Marseille', '13010', 'France'),
(7, 'Monsieur', 'Moustache', 'Mosst', 'Phillipe', 'moustache@gmail.com', '$2y$13$MtUDYOyz7HbbpCtSvlK3vOlOi6VGO9O3xqOWbjrhYWCixf1rIl/dG', '0506040102', '2018-06-27 10:44:10', 1, 'e1cba497ce2a417df1bf3d0d0c4645af.png', '78 boulevard des moustique', 'Orléans', '45000', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_8F02BF9DA76ED395` (`user_id`),
  KEY `IDX_8F02BF9DFE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user_group`
--

INSERT INTO `user_group` (`user_id`, `group_id`) VALUES
(2, 2),
(3, 3),
(4, 1),
(5, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `vente_p_a_p`
--

DROP TABLE IF EXISTS `vente_p_a_p`;
CREATE TABLE IF NOT EXISTS `vente_p_a_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `vente_p_a_p`
--

INSERT INTO `vente_p_a_p` (`id`, `titre`, `description`, `prix`, `ville`, `pays`, `code_postal`, `date_create`, `state`, `image`) VALUES
(1, 'Switch nintendo', 'Je vend une switch rouge', 250, 'Paris', 'France', '75012', '2018-06-26 13:44:24', 'review', NULL),
(2, 'PS4', 'Je vend ma ps4 car j\'en ai marre', 250, 'Paris', 'France', '75001', '2018-06-27 10:39:19', 'review', '3c75faa40ef2508e73d2ecf8c6de734d.jpeg');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `FK_8F02BF9DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_8F02BF9DFE54D947` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
