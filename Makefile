build:
	docker-compose build
up:
	docker-compose up -d
stop:
	docker-compose stop
rm:
	docker-compose rm -f
kill:
	docker-compose kill

logs:
	docker-compose logs

tail:
	docker-compose logs -f 


destroy:
	make stop
	make kill
	make rm

reboot:
	make stop
	make kill
	make up
	docker-compose exec genos php composer.phar update

install:
	make build
	make up
	docker-compose exec genos php composer.phar update
	make stop
	make up

reinstall:
	make destroy
	make install

#===========================#
#		 	GENOS			#
#===========================#
bash.genos:
	docker-compose exec genos bash

exec.genos:
	docker-compose exec genos

tail.genos:
	docker-compose logs -f genos

logs.genos:
	docker-compose logs genos

#===========================#
#		 	DB				#
#===========================#
bash.db:
	docker-compose exec database bash

exec.db:
	docker-compose exec database

tail.db:
	docker-compose logs -f database

logs.db:
	docker-compose logs database